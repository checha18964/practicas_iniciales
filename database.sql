CREATE TABLE Usuario(
 dpi bigint not null primary key,
 nombre varchar(30) not null,
 fecha_nacimiento date,
 correo varchar(30) not null,
 pass varchar(30) not null,
 permiso int null
);

CREATE TABLE Sede(
  cod_sede int not null primary key auto_increment,
  alias varchar(50) not null,
  direccion varchar(30) not null,
  departamento varchar(10) not null,
  municipio varchar(10) not null,
  encargado bigint not null,
  FOREIGN KEY(encargado) REFERENCES Usuario(dpi)
);

CREATE TABLE Rol(
	descripcion varchar(10) not null,
	cod_usuario bigint not null,
	FOREIGN key(cod_usuario) REFERENCES Usuario(dpi)
);

CREATE TABLE Categoria(
	descripcion varchar(20) not null,
	cod_producto varchar(50) not null,
	FOREIGN KEY(cod_producto) REFERENCES Producto(sku)
);


CREATE TABLE Bodega(
	cod_bodega int not null auto_increment primary key,
	nombre varchar(15) not null,
	direccion varchar(50) not null,
	estado int not null,
	cod_usuario bigint not null,
	cod_sede int not null,
	FOREIGN KEY(cod_usuario) REFERENCES Usuario(dpi),
	FOREIGN KEY(cod_sede) REFERENCES Sede(cod_sede)
);

CREATE TABLE Producto(
	sku varchar(50) not null primary key, 
	codigo_barras bigint not null, 
	nombre varchar(50) not null,
	descripcion varchar(200),
	precio float not null
);


CREATE TABLE Cliente(
	dpi bigint not null primary key,
	nombre varchar(30) not null,
	nit int not null,
	direccion varchar(50) not null,
	cod_sede int not null,
	FOREIGN KEY(cod_sede) REFERENCES Sede(cod_sede)
);

CREATE TABLE Venta(
	cod_venta int not null primary key auto_increment,
	fecha date not null,
	entrega date null, 
	estado int null, 
	tipo int not null,
	cargo float not null,
	descuento float not null,
	total float not null,
	vendedor bigint not null,
	cliente bigint not null,
	cod_sede int not null,
	repartidor bigint null,
	FOREIGN KEY(vendedor) REFERENCES Usuario(dpi),
	FOREIGN KEY(repartidor) REFERENCES Usuario(dpi),
	FOREIGN key(cliente) REFERENCES Cliente(dpi),
	FOREIGN KEY(cod_sede) REFERENCES Sede(cod_sede)
);

CREATE TABLE Detalle(
	cod_producto varchar(50) not null,
	cod_venta int not null,
	FOREIGN KEY(cod_producto) REFERENCES Producto(sku),
	FOREIGN KEY(cod_venta) REFERENCES Venta(cod_venta)
);

CREATE TABLE Inventario(
	cod_inventario int not null auto_increment primary key,
	cantigua int not null,
	cnueva int not null,
	motivo varchar(200) not null,
	fecha date not null,
	cod_usuario bigint not null,
	cod_bodega int not null,
	cod_sede int not null,
	cod_producto varchar(50) not null,
	FOREIGN key(cod_usuario) REFERENCES Usuario(dpi),
	FOREIGN key(cod_bodega) REFERENCES Bodega(cod_bodega),
	FOREIGN key(cod_sede) REFERENCES Sede(cod_sede),
	FOREIGN key(cod_producto) REFERENCES Producto(sku)
);
/*CREATE TABLE Transferencia(
	codigo int not null primary key auto_increment,
	tipo int not null,
	destino int,--pasar a int
	origen int,-- pasar a int
	estado int not null,
	descripcion varchar(50),
	cod_usuario int not null,
	repartidor int not null,
	recibe int not null,
	--agregar el repartidor y quien acepta transferencia
	FOREIGN KEY(cod_usuario) REFERENCES Usuario(dpi)
);*/

create table transferencia(
	cod_trans int not null primary key auto_increment,
	tipo int not null, --se refiere a interna o externa
	origen int not null,--se refiere a la bodega que lo solicita
	cod_sede int not null,
	estado int not null default 0,--se refiere a completa/incompleta
	cod_producto varchar(50) int not null, --producto a transferir
	cantidad int not null,
	aceptada int not null default 0;
	repartidor int null,
	recibe int null,
	envia int,  --se refiere a bodega que envia
	cod_sede2 int, --se refiere a sede que envia
	FOREIGN KEY(repartidor) REFERENCES Usuario(dpi),
	FOREIGN KEY(recibe) REFERENCES Usuario(dpi),
	FOREIGN KEY(cod_sede) REFERENCES Sede(cod_sede),
	FOREIGN KEY(cod_sede2) REFERENCES Sede(cod_sede),
	FOREIGN KEY(origen) REFERENCES Bodega(cod_bodega),
	FOREIGN KEY(envia) REFERENCES Bodega(cod_bodega),
	FOREIGN KEY(cod_producto) REFERENCES Producto(sku)
);