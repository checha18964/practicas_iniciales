import { Request, Response } from 'express'
import pool from '../database'
class IndexController {
    public async gethistorial(req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        const historial = await pool.query('SELECT * from historial where cod_atleta= ?', [id])
        if (historial.length > 0) {
            return res.json(historial);
        }
        res.status(404).send(false)
        console.log("El usuario no existe")
    }
    public async crearAtleta(req: Request, res: Response) {
        try {
            await pool.query('INSERT INTO Atleta set ?', [req.body]);
            //res.json({text:'usuario creado'});
            res.status(200).send(true)
            console.log(req.body)
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async crearEntrenador(req: Request, res: Response) {
        try {
            await pool.query('INSERT INTO Entrenador set ?', [req.body])
            //res.json({text:'usuario creado'});
            res.status(200).send(true)
            console.log(req.body)
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async deleteAtleta(req: Request, res: Response) {
        const { id } = req.params;
        try {
            await pool.query('delete from atleta where usuario= ?', [id])
            //res.send("Se elimino con exito")
            res.status(200).send(true)
            console.log("se elimino con exito")
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async deleteEntrenador(req: Request, res: Response) {
        const { id } = req.params;
        try {
            await pool.query('delete from entrenador where usuario= ?', [id])
            //res.send("Se elimino con exito")
            res.status(200).send(true)
            console.log("se elimino con exito")
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async updateAtleta(req: Request, res: Response) {
        const { id } = req.params;
        try {
            await pool.query('update atleta set ? where usuario= ?', [req.body, id])
            //res.send("Se actualizo con exito")
            res.status(200).send(true)
            console.log("se actualizo con exito")
        } catch (error) {
            res.status(404).send(false)
        }
        console.log("se actualizo con exito")
    }
    public async updateEntrenador(req: Request, res: Response) {
        const { id } = req.params;
        try {
            await pool.query('update entrenador set ? where usuario= ?', [req.body, id])
            //res.send("Se actualizo con exito")
            res.status(200).send(true)
            console.log("se actualizo con exito")
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async agregarMedicion(req: Request, res: Response) {
        try {
            await pool.query('INSERT INTO historial set ?', [req.body]);
            //res.json({text:'medicion guardada'});
            res.status(200).send(true)
            console.log(req.body)
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async loginAtleta(req: Request, res: Response) {
        const atleta = await pool.query('SELECT * from atleta where usuario= \'' + req.body.usuario + '\' and pass= \'' + req.body.pass + '\'')
        if (atleta.length > 0) {
            return res.send(true);
        }
        res.status(404).send(false)
        console.log("No se encontró el usuario")
    }
    public async loginEntrenador(req: Request, res: Response) {
        const entrenador = await pool.query('SELECT * from entrenador where usuario= \'' + req.body.usuario + '\' and pass= \'' + req.body.pass + '\'')
        if (entrenador.length > 0) {
            return res.json(true);
        }
        res.status(404).send(false)
        console.log("No se encontró el usuario")
    }
    public async getAtleta(req: Request, res: Response) {
        const { id } = req.params;
        const atleta = await pool.query('SELECT usuario from atleta where cod_entrenador= ?', [id])
        if (atleta.length > 0) {
            return res.json(atleta);
        }
        res.status(404).send(false)
        console.log("No se encontro ningun atleta asignado al entrenador")
    }

    public async getPromRC(req: Request, res: Response) {
        const { id } = req.params;
        try {
            const valor = await pool.query('select avg(rcardiaco) from historial where cod_atleta= ? group by cod_atleta', [id])
            //res.send(valor)
            res.status(200).send(valor)
            console.log(valor)
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async getPromOxigeno(req: Request, res: Response) {
        const { id } = req.params;
        try {
            const valor = await pool.query('select avg(oxigeno) from historial where cod_atleta= ? group by cod_atleta', [id])
            //res.send(valor)
            res.status(200).send(valor)
            console.log(valor)
        } catch (error) {
            res.status(404).send(false)
        }

    }
    public async getPromTemperatura(req: Request, res: Response) {
        const { id } = req.params;
        try {
            const valor = await pool.query('select avg(temperatura) from historial where cod_atleta= ? group by cod_atleta', [id])
            //res.send(valor)
            res.status(200).send(valor)
            console.log(valor)
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async getMaxRC(req: Request, res: Response) {
        const { id } = req.params;
        try {
            const valor = await pool.query('select max(rcardiaco) from historial where cod_atleta= ? group by cod_atleta', [id])
            // res.send(valor)
            res.status(200).send(valor)
            console.log(valor)
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async getMaxOxigeno(req: Request, res: Response) {
        const { id } = req.params;
        try {
            const valor = await pool.query('select max(oxigeno) from historial where cod_atleta= ? group by cod_atleta', [id])
            //res.send(valor)
            res.status(200).send(valor)
            console.log(valor)
        } catch (error) {
            res.status(404).send(false)
        }

    }
    public async getMaxTemperatura(req: Request, res: Response) {
        const { id } = req.params;
        try {
            const valor = await pool.query('select max(temperatura) from historial where cod_atleta= ? group by cod_atleta', [id])
            //res.send(valor)
            res.status(200).send(valor)
            console.log(valor)
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async getMinRC(req: Request, res: Response) {
        const { id } = req.params;
        try {
            const valor = await pool.query('select min(rcardiaco) from historial where cod_atleta= ? group by cod_atleta', [id])
            //res.send(valor)
            res.status(200).send(valor)
            console.log(valor)
        } catch (error) {
            res.status(404).send(false)
        }
    }
    public async getMinOxigeno(req: Request, res: Response) {
        const { id } = req.params;
        try {
            const valor = await pool.query('select min(oxigeno) from historial where cod_atleta= ? group by cod_atleta', [id])
            //res.send(valor)
            res.status(200).send(valor)
            console.log(valor)
        } catch (error) {
            res.status(404).send(false)
        }

    }
    public async getMinTemperatura(req: Request, res: Response) {
        const { id } = req.params;
        try {
            const valor = await pool.query('select min(temperatura) from historial where cod_atleta= ? group by cod_atleta', [id])
            //res.send(valor)
            res.status(200).send(valor)
            console.log(valor)
        } catch (error) {
            res.status(404).send(false)
        }
    }
}

export const indexController = new IndexController();